import { OrbitControls } from "three"

export function addOrbitControls (camera, el) {
  const controls = new OrbitControls( camera, el );
  controls.maxPolarAngle = Math.PI * 0.45;
  controls.enablePan = false  
}
